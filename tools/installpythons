#!/bin/bash
# -*- Mode:Shell; indent-tabs-mode:nil; tab-width -*-
#
# Copyright 2022 Kenneth Loafman <kenneth@loafman.com>
#
# This file is part of duplicity.
#
# Duplicity is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# Duplicity is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with duplicity; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

set -e

if [ "`uname`" != "Linux" ]; then
    echo "$0 does not run on `uname`"
    exit 2
fi

if [ "$#" -ne 0 ]; then
    echo "usage: $0"
    exit 2
fi

VERS="3.8 3.9 3.10 3.11 3.12"

# install python build environ
sudo apt update
sudo apt install -y build-essential libssl-dev zlib1g-dev \
                 libbz2-dev libreadline-dev libsqlite3-dev curl \
                 libncursesw5-dev xz-utils tk-dev libxml2-dev \
                 libxmlsec1-dev libffi-dev liblzma-dev

# install duplicity build environ
sudo apt install -y python3-dev librsync-dev gnupg

# install all versions of python and do initial setup
for V in ${VERS}; do
    pyenv install $V
    pyenv global $V
    python$V -m pip install -U build pip setuptools wheel
    python$V -m pip install -U -r requirements.txt
done
