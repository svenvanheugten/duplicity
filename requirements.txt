
##### installed first to avoid conflicts #####

chardet<3.10
cryptography==3.4.8
requests
urllib3<1.26

##### basic requirements #####

build>=1.1.1
fasteners>=0.19
pip>=24.0
python-gettext>=5.0
packaging>=20.0
setuptools>=69.1.1
wheel>=0.42.0

##### backend libraries #####

atom
azure-storage-blob
b2sdk
boto3
botocore
boxsdk[jwt]
dropbox
gdata-python3
google-api-python-client
google-auth-oauthlib
httplib2
jottalib
keyring
lxml
mediafire
megatools
paramiko
pexpect
psutil
pydrive2
pyrax
python-swiftclient
python-keystoneclient
requests-oauthlib

##### testing libraries #####

black==23.7.0
coverage
pycodestyle
pylint
pytest
pytest-cov
pytest-runner
tox

##### documentation libraries #####

gitchangelog
myst-parser
pystache
sphinx
sphinx-rtd-theme
